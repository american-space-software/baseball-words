import path from 'path'
import webpack from 'webpack'
const fs = require('fs');

const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const exec = require('child_process').exec;

const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}

const fileLoader = {
  loader: 'file-loader',
  options: {
    name: '[folder]/[name].[ext]'
  }
}


let mainConfig = {
  entry: './src/deploy.ts',
  mode: 'development',
  devtool: 'source-map',
  devServer: {
      contentBase: path.join(__dirname, 'public'),
      compress: true,
      port: 8082,
      injectClient: false
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
        use: [fileLoader],
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, 'framework7-loader'],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
    alias: {
      buffer: 'buffer'
    }
  },
  output: {
    filename: 'deploy.js',
    library: "deploy",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [

    new CleanWebpackPlugin({
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),

    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),


    //Deploy page for website
    new HtmlWebpackPlugin({
      inject: false,
      title: 'Deploy',
      // favicon: 'src/html/favicon.ico',
      template: 'src/html/deploy.html',
      filename: 'deploy.html'
    }),


  ]
}



export default mainConfig 





