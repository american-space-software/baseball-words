// @ts-nocheck


const BaseballWords = artifacts.require("BaseballWords")
const Words = artifacts.require("Words")
const Projects = artifacts.require("Projects")
const Baseballs = artifacts.require("Baseballs")
const TokenUri = artifacts.require("TokenUri")
const MockMLBCMultipleOwners = artifacts.require("MockMLBCMultipleOwners")


const assert = require('assert')
const truffleAssert = require('truffle-assertions')
import { BigNumber } from "@ethersproject/bignumber";

let user0
let user1
let user2
let user3
let user4


let mainContract
let wordsContract
let tokenUriContract
let mockMLBC
let baseballsContract

let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'

let projectId1 = '340282366920938463463374607431768211456'
let projectId2 = '680564733841876926926749214863536422912'
let projectId3 = '1020847100762815390390123822295304634368'

let user4NftBalance
let user2NftBalance

contract('BaseballWords - Mint', (accounts) => {

  before(async () => {

    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    mockMLBC = await MockMLBCMultipleOwners.new(user4,user2, { from: user0 })
    baseballsContract = await Baseballs.new(mockMLBC.address, { from: user0 })
    wordsContract = await Words.new()
    tokenUriContract = await TokenUri.new(wordsContract.address, { from: user0 })
    mainContract = await BaseballWords.new(wordsContract.address, baseballsContract.address, tokenUriContract.address, { from: user0 })

    await wordsContract.createWord('Theme', { from: user0 }) //1
    await wordsContract.createWord('Grass', { from: user0 }) //2
    await wordsContract.createWord('Bronze', { from: user0 }) //3
    await wordsContract.createWord('Silver', { from: user0 }) //4
    await wordsContract.createWord('Gold', { from: user0 }) //5
    await wordsContract.createWord('Fireworks', { from: user0 }) //6
    await wordsContract.createWord('American Flag', { from: user0 }) //7

    await wordsContract.createWord('Font Face', { from: user0 }) //8
    await wordsContract.createWord('Arial', { from: user0 }) //9
    await wordsContract.createWord('Courier New', { from: user0 }) //10
    await wordsContract.createWord('Georgia', { from: user0 }) //11
    await wordsContract.createWord('Times New Roman', { from: user0 }) //12
    await wordsContract.createWord('Verdana', { from: user0 }) //13

    await wordsContract.createWord('Font Size', { from: user0 }) //14
    await wordsContract.createWord('25', { from: user0 }) //15
    await wordsContract.createWord('35', { from: user0 }) //16
    await wordsContract.createWord('45', { from: user0 }) //17
    await wordsContract.createWord('55', { from: user0 }) //18

    await wordsContract.createWord('Font Style', { from: user0 }) //19
    await wordsContract.createWord('NORMAL', { from: user0 }) //20
    await wordsContract.createWord('ITALIC', { from: user0 }) //21
    await wordsContract.createWord('BOLD', { from: user0 }) //22
    await wordsContract.createWord('BOLDITALIC', { from: user0 }) //23


    //Create test project
    mainContract.createProject(
      ['The 714 Collection', "You're 24. You're a great pitcher but you're sick of only playing every 5 days and America just outlawed booze which is your favorite beverage. You tell the manager you want to hit. He puts you in the lineup but you can tell he's irritated. He wants you to pitch. You know you're a superstar if you can just get in the lineup every day. You start to dream about big apples.", "FAAAAAAAKE"],
      [50, web3.utils.toWei('10', 'ether')],
      [
        1, /**Theme*/
        8, /**Font Face */
        14, /**Font Size */
        19 /**Font Style */
      ],
      [
        [
          7, /** American Flag */
          15625,

          6, /** Fireworks */
          46875,

          5, /** Gold */
          62500,

          4, /** Silver */
          125000,

          3, /** Bronze*/
          250000,

          2, /**Grass*/
          500000
        ],
        [

          13, /** Verdana */
          31250,

          12, /** Times New Roman */
          93750,

          11, /** Georgia */
          125000,

          10, /** Courier New */
          250000,

          9, /** Arial */
          500000
        ],
        [
          18, /** 55 font size */
          62500,

          17, /** 45 font size */
          187500,

          16, /** 35 font size */
          250000,

          15, /** 25 font size */
          500000,

        ],
        [
          23, /** BOLDITALIC */
          62500,

          22, /** BOLD */
          187500,

          21, /** ITALIC */
          250000,

          20, /** NORMAL */
          500000,

        ]
      ]
    )


    mainContract.createProject(
      ['The 755 Collection', "You're 39. You just hit 40 home runs. Crazy. Who does that? You didn’t quite make it though. So close. Now you have the whole offseason to think about it and you joke that you just hope you don’t die before April. You don’t. You’re alive and you are going to make them remember your name. You’ve never stopped swinging. You start to dream about nails.", "FAAAAAAAKE"],
      [755, web3.utils.toWei('10', 'ether')],
      [
        1, /**Theme*/
        8, /**Font Face */
        14, /**Font Size */
        19 /**Font Style */
      ],
      [
        [
          7, /** American Flag */
          15625,

          6, /** Fireworks */
          46875,

          5, /** Gold */
          62500,

          4, /** Silver */
          125000,

          3, /** Bronze*/
          250000,

          2, /**Grass*/
          500000
        ],
        [

          13, /** Verdana */
          31250,

          12, /** Times New Roman */
          93750,

          11, /** Georgia */
          125000,

          10, /** Courier New */
          250000,

          9, /** Arial */
          500000
        ],
        [
          18, /** 55 font size */
          62500,

          17, /** 45 font size */
          187500,

          16, /** 35 font size */
          250000,

          15, /** 25 font size */
          500000,

        ],
        [
          23, /** BOLDITALIC */
          62500,

          22, /** BOLD */
          187500,

          21, /** ITALIC */
          250000,

          20, /** NORMAL */
          500000,

        ]
      ]
    )


    mainContract.createProject(
      ['The 762 Collection', "You’re 35. You have 3 MVPs but you’d have 6 if they didn’t hate you. They only care about home runs. You’d hit more but they literally won’t pitch to you. You’re shopping for clothes. You tell your wife that a walk and a stolen base is basically a double. She doesn’t care. Why does none of this stuff fit anymore she asks you. You tell her you’re trying a new diet and why does it even matter? Geez. You tell yourself you’re never going to hit another single. Home runs only. You start to have really weird dreams that you don’t want to tell the doctor about.", "FAAAAAAAKE"],
      [762, '10000000000000000000'],
      [
       1, /**Theme*/ 
       8, /**Font Face */
       14, /**Font Size */
       19 /**Font Style */
      ],
      [
          [
              7, /** American Flag */
              15625,

              6, /** Fireworks */
              46875,

              5, /** Gold */
              62500,

              4, /** Silver */
              125000,

              3, /** Bronze*/
              250000,

              2, /**Grass*/
              500000
          ],
          [

              13, /** Verdana */
              31250,

              12, /** Times New Roman */
              93750,

              11, /** Georgia */
              125000,

              10, /** Courier New */
              250000,

              9, /** Arial */
              500000
          ],
          [
              18, /** 55 font size */
              62500,
              
              17, /** 45 font size */
              187500,

              16, /** 35 font size */
              250000,

              15, /** 25 font size */
              500000,

          ],
          [
              23, /** BOLDITALIC */
              62500,

              22, /** BOLD */
              187500,

              21, /** ITALIC */
              250000,

              20, /** NORMAL */
              500000,

          ]
      ]
    )

    //Mint baseballs for user 4
    let tokens = [...Array(40).keys()]
    await baseballsContract.mint(tokens, { from: user4 })

 
    //Mint baseballs for user 2
    tokens = [...Array(2050).keys()].slice(2000) //2000-2050
    await baseballsContract.mint(tokens, { from: user2 })


    //Approve BaseballWords to spend Baseballs
    let maxApproval = '115792089237316195423570985008687907853269984665640564039457584007913129639935'; //(2^256 - 1 )
    await baseballsContract.approve(mainContract.address, maxApproval, { from: user4 })
    await baseballsContract.approve(mainContract.address, maxApproval, { from: user2 })

  })




  after(async () => {
  })


  /**
   * Mint
   */

  it('should fail to mint with zero project id', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.mint(
        '0',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid project id"
    )

  })

  it('should fail to mint with invalid project id', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.mint(
        '5',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid project"
    )

  })

  it('should fail to mint if it is not tagged as mintable', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.mint(
        '340282366920938463463374607431768211456',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Non-mintable project"
    )

  })

  it('should fail to activate project if not the owner', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.activateProject('340282366920938463463374607431768211456', { from: user1 }),
      truffleAssert.ErrorType.REVERT,
      "Ownable: caller is not the owner"
    )

  })

  it('should fail to activate project if not the owner', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.activateProject(projectId1, { from: user1 }),
      truffleAssert.ErrorType.REVERT,
      "Ownable: caller is not the owner"
    )

  })


  it('should activate projects', async () => {

    //Arrange
    let project1:Project = await mainContract.getProject(projectId1)
    let project2:Project = await mainContract.getProject(projectId2)
    let project3:Project = await mainContract.getProject(projectId3)

    assert.strictEqual(project1.mintable, false)
    assert.strictEqual(project2.mintable, false)
    assert.strictEqual(project3.mintable, false)

    //Act
    await mainContract.activateProject(projectId1, { from: user0 })
    await mainContract.activateProject(projectId2, { from: user0 })
    await mainContract.activateProject(projectId3, { from: user0 })

    //Assert
    project1 = await mainContract.getProject(projectId1)
    project2 = await mainContract.getProject(projectId2)
    project3 = await mainContract.getProject(projectId3)

    assert.strictEqual(project1.mintable, true)
    assert.strictEqual(project2.mintable, true)
    assert.strictEqual(project3.mintable, true)


  })

  it('should successfully mint multiple from first project', async () => {

    let tokenId1 = '340282366920938463463374607431768211457'
    let tokenId2 = '340282366920938463463374607431768211458'
    let tokenId3 = '340282366920938463463374607431768211459'
    let tokenId4 = '340282366920938463463374607431768211460'

    user4NftBalance = (await mainContract.balanceOf(user4)).toNumber()
    user2NftBalance = (await mainContract.balanceOf(user2)).toNumber()

    let tx = await mainContract.mint(projectId1, { from: user4 })


    //User 4
    await verifyMint(tx, projectId1, tokenId1, zeroAddress, user4, ++user4NftBalance, user4,
      web3.utils.toWei('80', 'ether'),
      web3.utils.toWei('30', 'ether'),
      1, //token index
      1, //total NFT supply
    )

    //User 4
    let tx2 = await mainContract.mint(projectId1, { from: user4 })
    await verifyMint(tx2, projectId1, tokenId2, zeroAddress, user4, ++user4NftBalance, user4,
      web3.utils.toWei('70', 'ether'),
      web3.utils.toWei('20', 'ether'),
      2, //token index
      2 //total NFT supply
    )


    //User 2
    let tx3 = await mainContract.mint(projectId1, { from: user2 })
    await verifyMint(tx3, projectId1, tokenId3, zeroAddress, user2, ++user2NftBalance, user2,
      web3.utils.toWei('60', 'ether'),
      web3.utils.toWei('40', 'ether'),
      3, //token index
      3 //total NFT supply
    )

    let tx4 = await mainContract.mint(projectId1, { from: user2 })
    await verifyMint(tx4, projectId1, tokenId4, zeroAddress, user2, ++user2NftBalance, user2,
      web3.utils.toWei('50', 'ether'),
      web3.utils.toWei('30', 'ether'),
      4, //token index
      4 //total NFT supply
    )


  })

  it('should successfully mint multiple from second project', async () => {

    let tokenId1 = '680564733841876926926749214863536422913'
    let tokenId2 = '680564733841876926926749214863536422914'
    let tokenId3 = '680564733841876926926749214863536422915'
    let tokenId4 = '680564733841876926926749214863536422916'

    user4NftBalance = (await mainContract.balanceOf(user4)).toNumber()
    user2NftBalance = (await mainContract.balanceOf(user2)).toNumber()

    let tx = await mainContract.mint(projectId2, { from: user4 })

    //User 4
    await verifyMint(tx, projectId2, tokenId1, zeroAddress, user4, ++user4NftBalance, user4,
      web3.utils.toWei('40', 'ether'),
      web3.utils.toWei('10', 'ether'),
      1, //token index
      5, //total NFT supply
    )

    //User 4
    let tx2 = await mainContract.mint(projectId2, { from: user4 })
    await verifyMint(tx2, projectId2, tokenId2, zeroAddress, user4, ++user4NftBalance, user4,
      web3.utils.toWei('30', 'ether'),
      web3.utils.toWei('0', 'ether'),
      2, //token index
      6 //total NFT supply
    )


    //User 2
    let tx3 = await mainContract.mint(projectId2, { from: user2 })
    await verifyMint(tx3, projectId2, tokenId3, zeroAddress, user2, ++user2NftBalance, user2,
      web3.utils.toWei('20', 'ether'),
      web3.utils.toWei('20', 'ether'),
      3, //token index
      7 //total NFT supply
    )

    let tx4 = await mainContract.mint(projectId2, { from: user2 })
    await verifyMint(tx4, projectId2, tokenId4, zeroAddress, user2, ++user2NftBalance, user2,
      web3.utils.toWei('10', 'ether'),
      web3.utils.toWei('10', 'ether'),
      4, //token index
      8 //total NFT supply
    )


  })

  it('should fail to mint if max supply is reached', async () => {

    //Arrange
    //Mint 1000 baseballs for user 2
    let tokens = [...Array(4000).keys()].slice(3000) //3000-4000
    await baseballsContract.mint(tokens, { from: user2 })


    user2NftBalance = (await mainContract.balanceOf(user2)).toNumber()
    
    let baseballsBalance = 1000
    let tokenId = BigNumber.from('340282366920938463463374607431768211461')

    for (let i=0; i < 46; i++) {

      let tx = await mainContract.mint(projectId1, { from: user2 })

      //User 4
      await verifyMint(tx, projectId1, tokenId, zeroAddress, user2, ++user2NftBalance, user2,
        web3.utils.toWei(baseballsBalance.toString(), 'ether'),
        web3.utils.toWei(baseballsBalance.toString(), 'ether'),
        i+5, //token index
        i+9, //total NFT supply
      )

      baseballsBalance -= 10
      tokenId = tokenId.add(1)

      let project = await mainContract.getProject(projectId1)

      console.log(`Minted ${project.minted} of ${project.maxSupply}`)
    }


    //Act
    await truffleAssert.fails(
      mainContract.mint(
        projectId1,
        { from: user2 }),
      truffleAssert.ErrorType.REVERT,
      "Max supply minted"
    )

  })

  it('should fail to mint if we dont have enough baseballs', async () => {

    web3.utils.toWei('9900', 'ether')

    //Create an expensive test project
    let receipt = await mainContract.createProject(
      ["My beautiful expensive project", "This is such a great project full of good stuff and it costs a lot", "xyz"],
      ['100', web3.utils.toWei('1000', 'ether')], //maxSupply: 2, burnFee: 10000 baseballs
      [1, 2, 3, 4],
      [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
      ],
      { from: user0 })

    //Activate it
    await mainContract.activateProject('680564733841876926926749214863536422912')

    //Act
    await truffleAssert.fails(
      mainContract.mint(
        '680564733841876926926749214863536422912',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Not enough balls"
    )

  })

  it('Should fail to get URI of invalid project', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.tokenURI(
        '4',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid token"
    )

  })

  it('Should fail to get URI of invalid token', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.tokenURI(
        '340282366920938463463374607431768211659',
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid token"
    )

  })

  it('Should always give an attribute roll from 0-1000000', async () => {

    //Act
    for (let i = 0; i < 300; i++) {
      let result = await mainContract.attributeRoll('340282366920938463463374607431768211459', i)
      assert.strictEqual(result.toNumber() <= 1000000, true)
    }

  })



})



function verifyTransferEvent(tx, id, from, to) {
  let eventCount = 0;
  for (let l of tx.logs) {
    if (l.event === 'Transfer') {
      try {
        assert.strictEqual(l.args.from, from)
        assert.strictEqual(l.args.to, to)
        assert.strictEqual(l.args.tokenId.toString(), id.toString())
        eventCount += 1
      } catch (ex) { }
    }
  }
  if (eventCount === 0) {
    assert(false, 'Missing Transfer Event')
  } else {
    assert(eventCount === 1, 'Unexpected number of Transfer events')
  }
}


async function verifyMint(tx,
  projectId,
  tokenId,
  from,
  to,
  nftBalance,
  operator,
  expectedTotalSupply,
  expectedBalance,
  tokenIndex,
  totalNftSupply
) {

  verifyTransferEvent(tx, tokenId, from, to)

  //Check totalsupply of baseballs. Should decrease by amount burned.
  let totalSupply = await baseballsContract.totalSupply()
  assert.strictEqual(totalSupply.toString(), expectedTotalSupply)

  //Check balance of baseballs
  let baseballsBalance = await baseballsContract.balanceOf(operator)
  assert.strictEqual(baseballsBalance.toString(), expectedBalance)

  //Validate project from token id
  let fetchedProjectId = await mainContract.tokenIdToProjectId(tokenId)
  assert.strictEqual(fetchedProjectId.toString(), projectId)

  //Get token index
  let fetchedTokenIndex = await mainContract.tokenIdToIndex(tokenId)
  assert.strictEqual(fetchedTokenIndex.toString(), tokenIndex.toString())

  //Validate tokenByProjectAndIndex
  let fetchedTokenByProjectAndIndex = await mainContract.tokenByProjectAndIndex(projectId, tokenIndex)
  assert.strictEqual(fetchedTokenByProjectAndIndex.toString(), tokenId.toString())

  //Validate totalNftSupply
  let fetchedTotalNftSupply = await mainContract.totalSupply()
  assert.strictEqual(fetchedTotalNftSupply.toString(), totalNftSupply.toString())

  //Get project details for comparisons
  let project = await mainContract.getProject(projectId)

  //Validate JSON
  let tokenJson = await getJson(tokenId)

  // console.log(tokenJson)

  //Validate name
  assert.strictEqual(tokenJson.name, `${project.name} #${fetchedTokenIndex.toString()}`)
  assert.strictEqual(tokenJson.attributes[0].value, `${project.name}`)




  let attributeCategories = await mainContract.projectAttributeCategories(projectId)
  let categoryIds = attributeCategories.map(ac => ac.toNumber())

  //Validate right number of attributes 
  assert.strictEqual(tokenJson.attributes.length - 1, categoryIds.length) //subtract one for the project name

  //Make sure there's a valid attribute for each category
  for (let categoryId of categoryIds) {

    let categoryName = await wordsContract.word(categoryId)

    let attributeId = await mainContract.tokenAttribute(categoryId, tokenId)
    let attributeName = await wordsContract.word(attributeId)

    //Filter to the right category in the JSON
    let jsonAttribute = tokenJson.attributes.filter(attr => attr.trait_type == categoryName)


    // console.log(`
    //   Category: ${categoryName} (${categoryId.toString()}) / ${jsonAttribute[0].trait_type}
    //   Attribute: ${attributeName} (${attributeId.toString()}) / ${jsonAttribute[0].value}
    // `)


    //Make sure the name is the same as what's in the token json
    assert.strictEqual(jsonAttribute[0].trait_type, categoryName)
    assert.strictEqual(jsonAttribute[0].value, attributeName)


    //Get the valid ones
    let attrProb = await mainContract.getAttributeProbabilities(projectId, categoryId)
    let ids = attrProb.map(ap => ap.attributeId)

    assert.strictEqual(ids.filter(id => id == attributeId).length, 1)

  }


  //Verify ownership
  let balance = await mainContract.balanceOf(operator)
  assert.strictEqual(balance.toString(), nftBalance.toString())

  let owner = await mainContract.ownerOf(tokenId)
  assert.strictEqual(owner.toString(), to.toString())



}


async function getJson(tokenId) {

  let uri = await mainContract.tokenURI(tokenId, { from: user0 })
  // console.log(uri)
  // 29 = length of "data:application/json;base64,"
  const json = Buffer.from(uri.substring(29), "base64");
  // console.log(json.toString())
  const result = JSON.parse(json.toString());

  return result

}







   // let burnFee = await projectsContract.getBurnFee(projectId1)
    // let startBalance = await baseballsContract.balanceOf(user4)
    // let totalSupply = await baseballsContract.totalSupply()
    
    // console.log(`
    //   Balance: ${web3.utils.fromWei(startBalance, 'ether')} balls, 
    //   Burn Fee: ${web3.utils.fromWei(burnFee, 'ether')},
    //   Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}
    // `)
