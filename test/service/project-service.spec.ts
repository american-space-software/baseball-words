// @ts-nocheck
import "reflect-metadata"

import {instance, mock, when} from "ts-mockito";

const BaseballWords = artifacts.require("BaseballWords");
const Words = artifacts.require("Words");
const Projects = artifacts.require("Projects");
const Baseballs = artifacts.require("Baseballs");
const TokenUri = artifacts.require("TokenUri")


const assert = require('assert')
const truffleAssert = require('truffle-assertions')

import { ProjectService } from "../../src/service/project-service"
import { QueueService } from "../../src/service/queue_service";
import { TokenService } from "../../src/service/token-service";
import { WalletService } from "../../src/service/wallet-service";

let user0
let user1
let user2
let user3
let user4


let mainContract
let wordsContract
let tokenUriContract

let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'


let projectService: ProjectService

contract('ProjectService', (accounts) => {

  before(async () => {

    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    wordsContract = await Words.new()
    tokenUriContract = await TokenUri.new(wordsContract.address, { from: user0 })
    mainContract = await BaseballWords.new(wordsContract.address, Baseballs.address, tokenUriContract.address, { from: user0 })



    await wordsContract.createWord("Font", { from: user0 })
    await wordsContract.createWord("Color", { from: user0 })
    await wordsContract.createWord("Size", { from: user0 })
    await wordsContract.createWord("Background", { from: user0 })
    await wordsContract.createWord("Weight", { from: user0 })

    await wordsContract.createWord("Helvetica12", { from: user0 })
    await wordsContract.createWord("Helvetica212", { from: user0 })
    await wordsContract.createWord("Verdana123", { from: user0 })
    await wordsContract.createWord("Arial123", { from: user0 })
    await wordsContract.createWord("Time123s", { from: user0 })

    await wordsContract.createWord("Blue", { from: user0 })
    await wordsContract.createWord("Red", { from: user0 })
    await wordsContract.createWord("Green", { from: user0 })
    await wordsContract.createWord("Yellow", { from: user0 })

    await wordsContract.createWord("Big", { from: user0 })
    await wordsContract.createWord("Real Big", { from: user0 })
    await wordsContract.createWord("Medium", { from: user0 })
    await wordsContract.createWord("Small", { from: user0 })
    await wordsContract.createWord("Real Smal", { from: user0 })

    await wordsContract.createWord("A monkey", { from: user0 })
    await wordsContract.createWord("Two birds", { from: user0 })
    await wordsContract.createWord("A dog", { from: user0 })
    await wordsContract.createWord("The Sun", { from: user0 })
    await wordsContract.createWord("Dog", { from: user0 })

    await wordsContract.createWord("100", { from: user0 })
    await wordsContract.createWord("200", { from: user0 })
    await wordsContract.createWord("300", { from: user0 })
    await wordsContract.createWord("400", { from: user0 })
    await wordsContract.createWord("500", { from: user0 })

    

    //Create  test project
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['2', web3.utils.toWei('100', 'ether')], //maxSupply: 2, burnFee: 100 baseballs
      [1,2,3,4],
      [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
      ],
      { from: user0 })


    await mainContract.createProject(
    ["My beautiful project2", "This is such a great project full of good stuff2.", "xyz"],
    ['2', web3.utils.toWei('100', 'ether')], //maxSupply: 2, burnFee: 100 baseballs
    [1,2,3,4],
    [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
    ],
    { from: user0 })


    await mainContract.createProject(
    ["My beautiful project", "This is such a great project full of good stuff3.", "xyz"],
    ['2', web3.utils.toWei('100', 'ether')], //maxSupply: 2, burnFee: 100 baseballs
    [1,2,3,4],
    [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
    ],
    { from: user0 })

    // Getting instance from mock
    let mockedWalletService:WalletService = mock(WalletService)
    when(mockedWalletService.getContract("Words")).thenReturn(wordsContract);
    when(mockedWalletService.getContract("BaseballWords")).thenReturn(mainContract);

    let walletService:WalletService = instance(mockedWalletService);



    projectService = new ProjectService(walletService, new QueueService({}), new TokenService(walletService))

  })


  after(async () => {
  })


  it('should get projects', async () => {

    let projects:Project[] = await projectService.getProjects()

    
    assert.strictEqual(projects.length, 3)

    assert.strictEqual(projects[0].description, "This is such a great project full of good stuff.")
    assert.strictEqual(projects[1].description, "This is such a great project full of good stuff2.")
    assert.strictEqual(projects[2].description, "This is such a great project full of good stuff3.")



  })


  it('should get project attributes', async () => {

    let projects:Project[] = await projectService.getProjects()
  
    let attributes:Attribute[] = await projectService.getProjectAttributes(projects[0].id)
    

    assert.strictEqual(attributes[0].category.id, 1)
    assert.strictEqual(attributes[1].category.id, 2)
    assert.strictEqual(attributes[2].category.id, 3)
    assert.strictEqual(attributes[3].category.id, 4)


    assert.strictEqual(attributes[0].attributeProbabilities[0].attribute.id, '4')
    assert.strictEqual(attributes[0].attributeProbabilities[0].probability, '400000')

    assert.strictEqual(attributes[0].attributeProbabilities[1].attribute.id, '3')
    assert.strictEqual(attributes[0].attributeProbabilities[1].probability, '300000')

    assert.strictEqual(attributes[0].attributeProbabilities[2].attribute.id, '2')
    assert.strictEqual(attributes[0].attributeProbabilities[2].probability, '200000')

    assert.strictEqual(attributes[0].attributeProbabilities[3].attribute.id, '1')
    assert.strictEqual(attributes[0].attributeProbabilities[3].probability, '100000')



    assert.strictEqual(attributes[1].attributeProbabilities[0].attribute.id, '7')
    assert.strictEqual(attributes[1].attributeProbabilities[0].probability, '900000')

    assert.strictEqual(attributes[1].attributeProbabilities[1].attribute.id, '6')
    assert.strictEqual(attributes[1].attributeProbabilities[1].probability, '100000')





    assert.strictEqual(attributes[2].attributeProbabilities[0].attribute.id, '12')
    assert.strictEqual(attributes[2].attributeProbabilities[0].probability, '500000')

    assert.strictEqual(attributes[2].attributeProbabilities[1].attribute.id, '11')
    assert.strictEqual(attributes[2].attributeProbabilities[1].probability, '400000')

    assert.strictEqual(attributes[2].attributeProbabilities[2].attribute.id, '10')
    assert.strictEqual(attributes[2].attributeProbabilities[2].probability, '100000')




    assert.strictEqual(attributes[3].attributeProbabilities[0].attribute.id, '17')
    assert.strictEqual(attributes[3].attributeProbabilities[0].probability, '400000')

    assert.strictEqual(attributes[3].attributeProbabilities[1].attribute.id, '15')
    assert.strictEqual(attributes[3].attributeProbabilities[1].probability, '300000')

    assert.strictEqual(attributes[3].attributeProbabilities[2].attribute.id, '16')
    assert.strictEqual(attributes[3].attributeProbabilities[2].probability, '300000')




  })

  it('should get NFTs for project by offset', async () => {
    
  })

})
