// @ts-nocheck
import "reflect-metadata"

import {instance, mock, when} from "ts-mockito";

const BaseballWords = artifacts.require("BaseballWords");
const Words = artifacts.require("Words");
const Projects = artifacts.require("Projects");
const MockMLBC = artifacts.require("MockMLBC")
const Baseballs = artifacts.require("Baseballs")
const TokenUri = artifacts.require("TokenUri")

const assert = require('assert')
const truffleAssert = require('truffle-assertions')

import { TokenService } from "../../src/service/token-service"
import { WalletService } from "../../src/service/wallet-service";

let user0
let user1
let user2
let user3
let user4


let mainContract
let wordsContract
let tokenUriContract
let baseballsContract
let mockMLBC

let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'
let projectId = '340282366920938463463374607431768211456'


let tokenService: TokenService

contract('TokenService', (accounts) => {

  before(async () => {

    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]
    
    mockMLBC = await MockMLBC.new(user0, { from: user0 })
    baseballsContract = await Baseballs.new(mockMLBC.address, { from: user0 })
    wordsContract = await Words.new()
    tokenUriContract = await TokenUri.new(wordsContract.address, { from: user0 })
    mainContract = await BaseballWords.new(wordsContract.address, baseballsContract.address, tokenUriContract.address, { from: user0 })


    await wordsContract.createWord("Font", { from: user0 })
    await wordsContract.createWord("Color", { from: user0 })
    await wordsContract.createWord("Size", { from: user0 })
    await wordsContract.createWord("Background", { from: user0 })
    await wordsContract.createWord("Weight", { from: user0 })

    await wordsContract.createWord("Helvetica12", { from: user0 })
    await wordsContract.createWord("Helvetica212", { from: user0 })
    await wordsContract.createWord("Verdana123", { from: user0 })
    await wordsContract.createWord("Arial123", { from: user0 })
    await wordsContract.createWord("Time123s", { from: user0 })

    await wordsContract.createWord("Blue", { from: user0 })
    await wordsContract.createWord("Red", { from: user0 })
    await wordsContract.createWord("Green", { from: user0 })
    await wordsContract.createWord("Yellow", { from: user0 })

    await wordsContract.createWord("Big", { from: user0 })
    await wordsContract.createWord("Real Big", { from: user0 })
    await wordsContract.createWord("Medium", { from: user0 })
    await wordsContract.createWord("Small", { from: user0 })
    await wordsContract.createWord("Real Smal", { from: user0 })

    await wordsContract.createWord("A monkey", { from: user0 })
    await wordsContract.createWord("Two birds", { from: user0 })
    await wordsContract.createWord("A dog", { from: user0 })
    await wordsContract.createWord("The Sun", { from: user0 })
    await wordsContract.createWord("Dog", { from: user0 })

    await wordsContract.createWord("100", { from: user0 })
    await wordsContract.createWord("200", { from: user0 })
    await wordsContract.createWord("300", { from: user0 })
    await wordsContract.createWord("400", { from: user0 })
    await wordsContract.createWord("500", { from: user0 })

 


    //Create  test project
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['2', web3.utils.toWei('10', 'ether')], //maxSupply: 2, burnFee: 10 baseballs
      [1,2,3,4],
      [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
      ],
      { from: user0 })

    let projectId = '340282366920938463463374607431768211456'
    await mainContract.activateProject(projectId, { from: user0 })


    //Mint baseballs for user
    await baseballsContract.mint([...Array(40).keys()], { from: user0 })

    //Approve BaseballWords to spend Baseballs
    let maxApproval = '115792089237316195423570985008687907853269984665640564039457584007913129639935'; //(2^256 - 1 )
    await baseballsContract.approve(mainContract.address, maxApproval, { from: user0 })


    await mainContract.mint(projectId, { from: user0 })
    await mainContract.mint(projectId, { from: user0 })

    // Getting instance from mock
    let mockedWalletService:WalletService = mock(WalletService)
    when(mockedWalletService.getContract("Words")).thenReturn(wordsContract);
    when(mockedWalletService.getContract("BaseballWords")).thenReturn(mainContract);

    let walletService:WalletService = instance(mockedWalletService);



    tokenService = new TokenService(walletService)

  })


  after(async () => {
  })


  it('should get token JSON', async () => {

    let tokenId1 = '340282366920938463463374607431768211457'
    let tokenMetadata:any = await tokenService.getMetadata(tokenId1)

    assert.strictEqual(tokenMetadata.name, "My beautiful project #1")
    assert.strictEqual(tokenMetadata.description, "This is such a great project full of good stuff.")
    assert.strictEqual(tokenMetadata.animation_url, "ipfs://xyz?t=340282366920938463463374607431768211457&a=1,1,2,7,3,10,4,17")
    assert.strictEqual(tokenMetadata.image_data, `<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 350 350' style='background:#56ab2f'><g><text font-size='5em' x='50%' y='40%' text-anchor='middle'>⚾️</text><text font-size='5em' x='50%' y='70%' text-anchor='middle'>Words</text><text font-size='3em' x='50%' y='90%' text-anchor='middle'>#1 / 2</text></g></svg>`)

  })

  it('should get token JSON by project and index', async () => {

    let tokenMetadata:any = await tokenService.getMetadataByProjectAndIndex(projectId, 1)
    let tokenMetadata2:any = await tokenService.getMetadataByProjectAndIndex(projectId, 2)


    assert.strictEqual(tokenMetadata.name, "My beautiful project #1")
    assert.strictEqual(tokenMetadata.description, "This is such a great project full of good stuff.")
    assert.strictEqual(tokenMetadata.animation_url, "ipfs://xyz?t=340282366920938463463374607431768211457&a=1,1,2,7,3,10,4,17")
    assert.strictEqual(tokenMetadata.image_data, `<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 350 350' style='background:#56ab2f'><g><text font-size='5em' x='50%' y='40%' text-anchor='middle'>⚾️</text><text font-size='5em' x='50%' y='70%' text-anchor='middle'>Words</text><text font-size='3em' x='50%' y='90%' text-anchor='middle'>#1 / 2</text></g></svg>`)

    

    assert.strictEqual(tokenMetadata2.name, "My beautiful project #2")
    assert.strictEqual(tokenMetadata2.description, "This is such a great project full of good stuff.")
    assert.strictEqual(tokenMetadata2.animation_url, "ipfs://xyz?t=340282366920938463463374607431768211458&a=1,4,2,7,3,11,4,17")
    assert.strictEqual(tokenMetadata2.image_data, `<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 350 350' style='background:#56ab2f'><g><text font-size='5em' x='50%' y='40%' text-anchor='middle'>⚾️</text><text font-size='5em' x='50%' y='70%' text-anchor='middle'>Words</text><text font-size='3em' x='50%' y='90%' text-anchor='middle'>#2 / 2</text></g></svg>`)

  })

  it('should get token list', async () => {

    let list:[] = await tokenService.getProjectTokenList(projectId, 0)
    console.log(list)
    assert.strictEqual(list.length, 2)

  })



})
