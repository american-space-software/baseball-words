// @ts-nocheck

const MockMLBC = artifacts.require("MockMLBC")
const MockMLBCFull = artifacts.require("MockMLBCFull")

const Baseballs = artifacts.require("Baseballs");

const assert = require('assert')
const truffleAssert = require('truffle-assertions')


let user0
let user1
let user2
let user3
let user4


let mainContract
let wordsContract

let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'

let mockMLBC 

contract('Baseballs', (accounts) => {

    
    before(async function() {

        this.timeout(30000000000)


        user0 = accounts[0]
        user1 = accounts[1]
        user2 = accounts[2]
        user3 = accounts[3]
        user4 = accounts[4]

        mockMLBC = await MockMLBC.new(user0)
        mainContract = await Baseballs.new(mockMLBC.address)

    })

    after(async () => {
    })


    it('should fail to mint with single invalid MLBC token: high', async () => {

        //Act
        await truffleAssert.fails(
            mainContract.mint([249173], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Token too high"
        )

    })

    it('should fail to mint with single token not owned by user', async () => {

        //Act
        await truffleAssert.fails(
            mainContract.mint([51], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Not token owner"
        )

    })

    it('should mint one token', async () => {

        //Act
        await mainContract.mint([49], { from: user0 })

        //Assert
        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()
        let baseballMinted = await mainContract.isMinted(49)
        let totalMinted = await mainContract.totalMinted()
        
        console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)

        assert.strictEqual(balance.toString(), '1000000000000000000')
        assert.strictEqual(totalSupply.toString(), '1000000000000000000')
        assert.strictEqual(baseballMinted, true)
        assert.strictEqual(totalMinted.toNumber(), 1)

    })

    it('should do a simple transfer', async () => {

        await mainContract.transfer(user1, '25000')

        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()

        assert.strictEqual(balance.toString(), '999999999999975000')
        assert.strictEqual(totalSupply.toString(), '1000000000000000000')
    })

    it('should fail to mint with single token that is already minted', async () => {

        //Act
        await truffleAssert.fails(
            mainContract.mint([49], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Already minted"
        )

    })

    it('should fail to mint with invalid MLBC token in group', async () => {

        //Act
        await truffleAssert.fails(
            mainContract.mint([1, 249173], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Token too high"
        )
    })

    it('should fail to mint with single token not owned by user in group', async () => {

        //Act
        await truffleAssert.fails(
            mainContract.mint([1, 51], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Not token owner"
        )

    })

    it('should fail to mint with single token that is already minted in group', async () => {
        //Act
        await truffleAssert.fails(
            mainContract.mint([1, 49], { from: user0 }),
            truffleAssert.ErrorType.REVERT,
            "Already minted"
        )
    })

    it('should mint 20 tokens at once', async () => {

        let ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

        //Act
        let receipt = await mainContract.mint(ids, { from: user0 })
        console.log(`GasUsed: ${receipt.receipt.gasUsed}`);

        //Assert
        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()
        let totalMinted = await mainContract.totalMinted()


        console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)

        for (let id of ids) {
            let baseballMinted = await mainContract.isMinted(id)
            assert.strictEqual(baseballMinted, true)
        }

        assert.strictEqual(balance.toString(), '20999999999999975000')
        assert.strictEqual(totalSupply.toString(), '21000000000000000000')
        assert.strictEqual(totalMinted.toNumber(), 21)


    })

    it('should mint 100 tokens at once', async () => {

        //Arrange
        let ids = []
        for (let i = 1000; i < 1100; i++) {
            ids.push(i)
        }

        //Act
        let receipt = await mainContract.mint(ids, { from: user0 })
        console.log(`GasUsed: ${receipt.receipt.gasUsed}`);

        //Assert
        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()
        let totalMinted = await mainContract.totalMinted()


        console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)


        assert.strictEqual(balance.toString(), '120999999999999975000')
        assert.strictEqual(totalSupply.toString(), '121000000000000000000')
        assert.strictEqual(totalMinted.toNumber(), 121)


    })

    // it('should mint one consecutive token', async () => {

    //     //Arrange
    //     //Start with a clean contract
    //     let mockMLBCFull = await MockMLBCFull.new(user0)
    //     mainContract = await Baseballs.new(mockMLBCFull.address)

    //     //Act
    //     await mainContract.mintConsecutive(4,4, { from: user0 })

    //     //Assert
    //     let balance = await mainContract.balanceOf(user0)
    //     let totalSupply = await mainContract.totalSupply()
    //     let baseballMinted = await mainContract.isMinted(4)
    //     let totalMinted = await mainContract.totalMinted()

        
    //     console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)

    //     assert.strictEqual(balance.toString(), '1000000000000000000')
    //     assert.strictEqual(totalSupply.toString(), '1000000000000000000')
    //     assert.strictEqual(baseballMinted, true)
    //     assert.strictEqual(totalMinted.toNumber(), 1)


    // })

    // it('should mint two consecutive token', async () => {

    //     //Arrange
    //     //Start with a clean contract
    //     let mockMLBCFull = await MockMLBCFull.new(user0)
    //     mainContract = await Baseballs.new(mockMLBCFull.address)

    //     //Act
    //     await mainContract.mintConsecutive(4,5, { from: user0 })

    //     //Assert
    //     let balance = await mainContract.balanceOf(user0)
    //     let totalSupply = await mainContract.totalSupply()
    //     let baseballMinted = await mainContract.isMinted(4)
    //     let baseballMinted2 = await mainContract.isMinted(5)
    //     let totalMinted = await mainContract.totalMinted()


    //     console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)

    //     assert.strictEqual(balance.toString(), '2000000000000000000')
    //     assert.strictEqual(totalSupply.toString(), '2000000000000000000')
    //     assert.strictEqual(baseballMinted, true)
    //     assert.strictEqual(baseballMinted2, true)
    //     assert.strictEqual(totalMinted.toNumber(), 2)

    // })

    it('should mint groups of 1000 consecutive tokens', async () => {

        //Arrange
        //Start with a clean contract
        let mockMLBCFull = await MockMLBCFull.new(user0)
        mainContract = await Baseballs.new(mockMLBCFull.address)

        let starts = []
        for (let i=1; i < 50000; i++) {
            if (i % 1000 == 0) {
                starts.push(i)
            }
        }
        

        //Randomize order of starts
        shuffleArray(starts)
        
        //Act
        for (let start of starts) {

             let end = start+999

             let tokenIds = []

             for (let i=start; i <= end; i++) {
                 tokenIds.push(i)
             }

             
             console.log(`Minting ${start} - ${end}`)
             let receipt = await mainContract.mint(tokenIds, { from: user0 })
             console.log(`GasUsed: ${receipt.receipt.gasUsed}`);

             let balance = await mainContract.balanceOf(user0)
             let totalSupply = await mainContract.totalSupply()

            //  for (let i=start; i<=end; i++) {
            //     let baseballMinted = await mainContract.isMinted(i)
            //     assert.strictEqual(baseballMinted, true)
            //  }
     
             console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)
        }
        
        //Assert

        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()
        let totalMinted = await mainContract.totalMinted()


        assert.strictEqual(balance.toString(), '49000000000000000000000')
        assert.strictEqual(totalSupply.toString(), '49000000000000000000000')
        assert.strictEqual(totalMinted.toNumber(), 49000)

    })

    // it('should fail to mint consecutive with a single invalid token', async () => {

    //     //Arrange
    //     //Start with a clean contract
    //     let mockMLBCFull = await MockMLBCFull.new(user0)
    //     mainContract = await Baseballs.new(mockMLBCFull.address)

    //     //Act
    //     await truffleAssert.fails(
    //         mainContract.mintConsecutive(248573, 249173, { from: user0 }),
    //         truffleAssert.ErrorType.REVERT,
    //         "Token too high"
    //     )

    // })

    it('should mint groups of 100 non-consecutive tokens', async() => {

        //Arrange

        //Start with a clean contract and mint every token
        let mockMLBCFull = await MockMLBCFull.new(user0)
        mainContract = await Baseballs.new(mockMLBCFull.address)

        //Put all the ids in an array
        let ids = []
        for (let i=1; i < 249173; i++) {
            ids.push(i)
        }

        //Randomize the order
        shuffleArray(ids)

        //Mint all
        const n = 100

        const resultArrays = new Array(Math.ceil(ids.length / n))
          .fill()
          .map(_ => ids.splice(0, n))

        //Act
        let count=0
        for (let arr of resultArrays) {

            let receipt = await mainContract.mint(arr, { from: user0 })
            console.log(`Batch ${count} GasUsed: ${receipt.receipt.gasUsed}`);
            count++

            if (count == 100) break; //ok that's enough

        }
      
        
        //Assert
        let balance = await mainContract.balanceOf(user0)
        let totalSupply = await mainContract.totalSupply()
        let totalMinted = await mainContract.totalMinted()


        console.log(`Balance: ${web3.utils.fromWei(balance, 'ether')} balls, Total Supply: ${web3.utils.fromWei(totalSupply, 'ether')}`)


        //Assert
        assert.strictEqual(balance.toString(), '10000000000000000000000')
        assert.strictEqual(totalSupply.toString(), '10000000000000000000000')
        assert.strictEqual(totalMinted.toNumber(), 10000)

    })


})



function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}