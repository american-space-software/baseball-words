// @ts-nocheck
const Words = artifacts.require("Words");

const assert = require('assert')
const truffleAssert = require('truffle-assertions')


let user0
let user1
let user2
let user3
let user4


let mainContract


let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'


contract('Words', (accounts) => {

  before(async () => {
    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]
    mainContract = await Words.new()
  })


  after(async () => {
  })



  /**
   * Create word
   */



  it('should fail to createWord with a zero length string', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createWord("", { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Text is empty"
    )

  })

  it('should createWord', async () => {

    await mainContract.createWord("Helvetica", { from: user0 })

    let result = await mainContract.wordId("Helvetica");

    assert.strictEqual(result.toNumber(), 1)

  })

  it('should createWord with non-owner', async () => {

    await mainContract.createWord("Helvetica2", { from: user4 })

    let result = await mainContract.wordId("Helvetica2");

    assert.strictEqual(result.toNumber(), 2)

  })

  it('should fail to createWord with a duplicate name', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createWord("Helvetica", { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Word already exists"
    )

  })

  it('should get wordId for invalid word', async () => {

    let result = await mainContract.wordId("dddd");
    assert.strictEqual(result.toNumber(), 0)

  })


  it('should createWords', async () => {

    await mainContract.createWords(["Word1", "Word2", "Word3", "Word4"], { from: user4 })

    let result1 = await mainContract.wordId("Word1");
    let result2 = await mainContract.wordId("Word2");
    let result3 = await mainContract.wordId("Word3");
    let result4 = await mainContract.wordId("Word4");

    assert.strictEqual(result1.toNumber(), 3)
    assert.strictEqual(result2.toNumber(), 4)
    assert.strictEqual(result3.toNumber(), 5)
    assert.strictEqual(result4.toNumber(), 6)

  })




})


// async function createAttributeAndAssert(attribute, user, id) {
//   await mainContract.createAttribute(attribute, { from: user })
//   let result = await mainContract.attributeIdByName(attribute);
//   assert.strictEqual(result.toNumber(), id)
// }

