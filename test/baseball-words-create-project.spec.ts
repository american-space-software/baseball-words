// @ts-nocheck


const BaseballWords = artifacts.require("BaseballWords");
const Words = artifacts.require("Words");
const Projects = artifacts.require("Projects");
const Baseballs = artifacts.require("Baseballs");
const TokenUri = artifacts.require("TokenUri")

const assert = require('assert')
const truffleAssert = require('truffle-assertions')


let user0
let user1
let user2
let user3
let user4


let mainContract
let wordsContract
let tokenUriContract

let tx

let zeroAddress = '0x0000000000000000000000000000000000000000'


contract('BaseballWords - Create Project', (accounts) => {

  before(async () => {

    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    wordsContract = await Words.new()
    tokenUriContract = await TokenUri.new(wordsContract.address, { from: user0 })
    mainContract = await BaseballWords.new(wordsContract.address, Baseballs.address, tokenUriContract.address, { from: user0 })

    await wordsContract.createWord("Font", { from: user0 })
    await wordsContract.createWord("Color", { from: user0 })
    await wordsContract.createWord("Size", { from: user0 })
    await wordsContract.createWord("Background", { from: user0 })
    await wordsContract.createWord("Weight", { from: user0 })

    await wordsContract.createWord("Helvetica", { from: user0 })
    await wordsContract.createWord("Helvetica2", { from: user0 })
    await wordsContract.createWord("Verdana", { from: user0 })
    await wordsContract.createWord("Arial", { from: user0 })
    await wordsContract.createWord("Times", { from: user0 })

    await wordsContract.createWord("Blue", { from: user0 })
    await wordsContract.createWord("Red", { from: user0 })
    await wordsContract.createWord("Green", { from: user0 })
    await wordsContract.createWord("Yellow", { from: user0 })

    await wordsContract.createWord("Big", { from: user0 })
    await wordsContract.createWord("Real Big", { from: user0 })
    await wordsContract.createWord("Medium", { from: user0 })
    await wordsContract.createWord("Small", { from: user0 })
    await wordsContract.createWord("Real Smal", { from: user0 })

    await wordsContract.createWord("A monkey", { from: user0 })
    await wordsContract.createWord("Two birds", { from: user0 })
    await wordsContract.createWord("A dog", { from: user0 })
    await wordsContract.createWord("The Sun", { from: user0 })
    await wordsContract.createWord("Babe Ruth", { from: user0 })

    await wordsContract.createWord("100", { from: user0 })
    await wordsContract.createWord("200", { from: user0 })
    await wordsContract.createWord("300", { from: user0 })
    await wordsContract.createWord("400", { from: user0 })
    await wordsContract.createWord("500", { from: user0 })

    

    //Create test project
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['2', web3.utils.toWei('100', 'ether')], //maxSupply: 2, burnFee: 100 baseballs
      [1,2,3,4],
      [
        [1, 100000, 2, 200000, 3, 300000, 4, 400000],
        [6, 100000, 7, 900000],
        [10, 100000, 11, 400000, 12, 500000],
        [15, 300000, 16, 300000, 17, 400000],
      ],
      { from: user0 })

  })


  after(async () => {
  })


  /**
   * Create project
   */
  it('should fail to createProject with non-owner', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 100, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user4 }),
      truffleAssert.ErrorType.REVERT,
      "Ownable: caller is not the owner"
    )

  })


  it('should fail to createProject with zero length name', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["", "This is such a great project full of good stuff.", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 100, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Name is empty"
    )

  })

  it('should fail to createProject with zero length description', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 100, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Description is empty"
    )

  })

  it('should fail to createProject with zero maxSupply', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['0', web3.utils.toWei('100', 'ether')], //maxSupply: 0, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Max supply is zero"
    )

  })

  it('should fail to createProject with zero length IPFS ', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", ""],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "IPFS is empty"
    )

  })

  it('should fail to createProject with zero burnFee ', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', '0'], //maxSupply: 100, burnFee: 0  baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid burnFee"
    )

  })

  it('should fail to createProject with empty attribute categories ', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [],
        [
          [1, 1000, 2, 2000],
          [3, 100, 4, 200]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "No attribute categories"
    )

  })

  it('should fail to createProject with too few attributes ', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Input size mismatch"
    )

  })

  it('should fail to createProject with too many attributes ', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1, 2],
        [
          [1, 1000, 2, 2000],
          [1, 1000, 2, 2000],
          [1, 1000, 2, 2000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Input size mismatch"
    )

  })

  it('should fail to createProject with an invalid attribute category', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [300],
        [
          [1, 1000, 2, 2000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid attribute category"
    )

  })

  it('should fail to createProject with an invalid attribute', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1],
        [
          [2, 1000, 60, 2000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid attribute"
    )

  })

  it('should fail to createProject with an invalid probability: zero', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1],
        [
          [1, 0]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid probability"
    )

  })

  it('should fail to createProject with an invalid probability total: low', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1],
        [
          [1, 100000, 2, 200000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid probability total"
    )

  })

  it('should fail to createProject with an invalid probability total: high', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1],
        [
          [1, 2000000, 2, 2000000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid probability total"
    )

  })

  it('should fail to createProject with a single invalid probability total', async () => {

    //Arrange
    let originalAc = [1, 2, 3, 4]
    let originalAp = [
      [1, 100000, 2, 200000, 3, 300000, 4, 4000000],
      [6, 9000000, 7, 1000000],
      [10, 1000000, 11, 5000000, 12, 4000000],
      [15, 200000, 16, 300000, 17, 400000],
    ]

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 10000, burnFee: 100 baseballs
        originalAc,
        originalAp,
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Invalid probability total"
    )

  })

  it('should fail to createProject if probabilities are not ascending', async () => {

    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 10000, burnFee: 100 baseballs
        [1, 2, 3, 4],
        [
          [1, 300000, 2, 200000, 3, 100000, 4, 400000],
          [6, 900000, 7, 100000],
          [10, 100000, 11, 500000, 12, 400000],
          [15, 300000, 16, 300000, 17, 400000],
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Descending probability"
    )


    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 10000, burnFee: 100 baseballs
        [1, 2, 3, 4],
        [
          [1, 100000, 2, 200000, 3, 300000, 4, 400000],
          [6, 900000, 7, 100000],
          [10, 100000, 11, 500000, 12, 400000],
          [15, 300000, 16, 300000, 17, 400000],
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Descending probability"
    )

  })

  it('should createProject', async () => {

    //Act
    let tx = await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      [1],
      [
        [1, 1000000]
      ],
      { from: user0 })

    let PROJECT_ID = '680564733841876926926749214863536422912'

    //Check tokenToProject
    let projectId = await mainContract.tokenIdToProjectId(PROJECT_ID)
    assert.strictEqual(projectId.toString(), PROJECT_ID)

    //Check projectId by index
    let projectId2 = await mainContract.projectIds(1)
    let projectCount = await mainContract.projectCount()
    assert.strictEqual(projectId.toString(), projectId2.toString())
    assert.strictEqual(projectCount.toString(), '2')


    //Check directly on project contract
    let result = await mainContract.getProject(projectId)
    assert.strictEqual(result.id.toString(), projectId.toString())
    assert.strictEqual(result.name, "My beautiful project")
    assert.strictEqual(result.description, "This is such a great project full of good stuff.")
    assert.strictEqual(result.maxSupply, '1')
    assert.strictEqual(result.burnFee.toString(), web3.utils.toWei('100', 'ether'))
    assert.strictEqual(result.minted, '0')
    assert.strictEqual(result.ipfs, "xyz")
    assert.strictEqual(result.mintable, false)
    assert.deepEqual(result.attributeCategoryIds, [1])

  })

  it('should createProject a second time', async () => {

    //Act
    let tx = await mainContract.createProject(
      ["My beautiful second project", "This is such a great project full of more good stuff.", "xyz"],
      ['1', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      [1],
      [
        [1, 1000000]
      ],
      { from: user0 })

    let PROJECT_ID = '1020847100762815390390123822295304634368'

    let projectId = await mainContract.tokenIdToProjectId(PROJECT_ID)
    assert.strictEqual(projectId.toString(), PROJECT_ID)

    //Check projectId by index
    let projectId2 = await mainContract.projectIds(2)
    let projectCount = await mainContract.projectCount()
    assert.strictEqual(projectId.toString(), projectId2.toString())
    assert.strictEqual(projectCount.toString(), '3')


    //Check directly on project contract
    let result = await mainContract.getProject(projectId)
    assert.strictEqual(result.id.toString(), projectId.toString())
    assert.strictEqual(result.name, "My beautiful second project")
    assert.strictEqual(result.description, "This is such a great project full of more good stuff.")
    assert.strictEqual(result.maxSupply, '1')
    assert.strictEqual(result.burnFee.toString(), web3.utils.toWei('100', 'ether'))
    assert.strictEqual(result.minted, '0')
    assert.strictEqual(result.ipfs, "xyz")
    assert.strictEqual(result.mintable, false)
    assert.deepEqual(result.attributeCategoryIds, [1])

  })

  it('should createProject with 5 attribute categories', async () => {

    //Arrange
    let originalAc = [1, 2, 3, 4]
    let originalAp = [
      [1, 100000, 2, 200000, 3, 300000, 4, 400000],
      [6, 100000, 7, 900000],
      [10, 100000, 11, 400000, 12, 500000],
      [15, 300000, 16, 300000, 17, 400000],
    ]


    //Act
    let tx = await mainContract.createProject(
      ["My beautiful third project", "Wow such beauty", "xyz"],
      ['100', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      originalAc,
      originalAp,
      { from: user0 })

    let PROJECT_ID = '1361129467683753853853498429727072845824'

    //Assert
    let projectId = await mainContract.tokenIdToProjectId(PROJECT_ID)
    assert.strictEqual(projectId.toString(), PROJECT_ID)

    //Check projectId by index
    let projectId2 = await mainContract.projectIds(3)
    let projectCount = await mainContract.projectCount()
    assert.strictEqual(projectId.toString(), projectId2.toString())
    assert.strictEqual(projectCount.toString(), '4')

    //Get project struct
    let project = await mainContract.getProject(PROJECT_ID)

    assert.strictEqual(project.id.toString(), PROJECT_ID)
    assert.strictEqual(project.name, 'My beautiful third project')
    assert.strictEqual(project.description, 'Wow such beauty')
    assert.strictEqual(project.maxSupply.toString(), '100')
    assert.strictEqual(project.burnFee.toString(), web3.utils.toWei('100', 'ether'))
    assert.strictEqual(project.ipfs, 'xyz')
    assert.strictEqual(project.mintable, false)


    //Get project struct directly from project contract
    project = await mainContract.getProject(PROJECT_ID)

    assert.strictEqual(project.id.toString(), PROJECT_ID)
    assert.strictEqual(project.name, 'My beautiful third project')
    assert.strictEqual(project.description, 'Wow such beauty')
    assert.strictEqual(project.maxSupply.toString(), '100')
    assert.strictEqual(project.ipfs, 'xyz')
    assert.strictEqual(project.mintable, false)



    //Get the attribute categories and validate
    let attributeCategories = await mainContract.projectAttributeCategories(PROJECT_ID)
    let acArray = attributeCategories.map(ac => ac.toNumber())

    assert.deepEqual(acArray, originalAc)

    //Get the attributes and probabilities
    for (let i = 0; i < acArray.length; i++) {
      let ap = await mainContract.getAttributeProbabilities(project.id.toString(), acArray[i])
      verifyAttributeProbabilities(ap, originalAp[i])
    }

  })

  it('should fail to create a project where the burn fee puts us past the max', async () => {

    //Reset contract
    mainContract = await BaseballWords.new(Words.address, Baseballs.address, tokenUriContract.address, { from: user0 })

    //Ok we need to get up to 249172 ether worth. Then make another.

    //100k
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['1000', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      [1],
      [
        [1, 1000000]
      ],
      { from: user0 })

    //200k
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['1000', web3.utils.toWei('100', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      [1],
      [
        [1, 1000000]
      ],
      { from: user0 })


    //49172
    await mainContract.createProject(
      ["My beautiful project", "This is such a great project full of good stuff.", "xyz"],
      ['1000', web3.utils.toWei('49.172', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
      [1],
      [
        [1, 1000000]
      ],
      { from: user0 })


    //Act
    await truffleAssert.fails(
      mainContract.createProject(
        ["My Beautiful Project", "This is such a great project full of good stuff.", "xyz"],
        ['1', web3.utils.toWei('1', 'ether')], //maxSupply: 1, burnFee: 100 baseballs
        [1],
        [
          [2, 1000, 5, 999000]
        ],
        { from: user0 }),
      truffleAssert.ErrorType.REVERT,
      "Burn fee too high"
    )

  })


    //should create a project with 10 attributes


})


async function createWordAndAssert(word, user, id) {
  await wordsContract.createWord(word, { from: user })
  let result = await wordsContract.wordId(word);
  assert.strictEqual(result.toNumber(), id)
}

function verifyAttributeProbabilities(attributeProbabilities, expected) {

  let i = 0
  for (let ap of attributeProbabilities) {

    assert.strictEqual(ap.attributeId, expected[i].toString())
    i++

    assert.strictEqual(ap.probability, expected[i].toString())
    i++

  }

  //Make sure there's not extra attributeProbabilities back
  assert.strictEqual(attributeProbabilities.length, expected.length / 2)

}

function verifyTransferEvent(tx, id, from, to) {

  let eventCount = 0;

  for (let l of tx.logs) {
      if (l.event === 'Transfer') {
          try {
              assert.strictEqual(l.args.from, from)
              assert.strictEqual(l.args.to, to)
              assert.strictEqual(l.args.tokenId.toString(), id.toString())
              eventCount += 1
          } catch (ex) { }
      }
  }
  if (eventCount === 0) {
      assert(false, 'Missing Transfer Event')
  } else {
      assert(eventCount === 1, 'Unexpected number of Transfer events')
  }
}


async function verifyMint(tx,
  projectId,
  tokenId,
  from,
  to,
  quantity,
  operator,
  expectedTotalSupply,
  expectedBalance
) {

  verifyTransferEvent(tx, tokenId, from, to)

  //Check totalsupply of baseballs. Should decrease by amount burned.
  let totalSupply = await mockBaseballs.totalSupply()
  assert.strictEqual(totalSupply.toString(), expectedTotalSupply)

  //Check balance of baseballs
  let baseballsBalance = await mockBaseballs.balanceOf(from)
  assert.strictEqual(baseballsBalance.toString(), expectedBalance)

  //Validate project from token id
  let fetchedProjectId = await mainContract.tokenIdToProjectId(tokenId)
  assert.strictEqual(fetchedProjectId.toString(), projectId)

  //Get token index
  let fetchedTokenIndex = await mainContract.tokenToIndex(tokenId)


  //Get project details for comparisons
  let project = await mainContract.getProject(projectId)

  //Validate JSON
  let tokenJson = await getJson(tokenId)

  //Validate name
  assert.strictEqual(tokenJson.name, `${project.name} #${fetchedTokenIndex.toString()}`)
  assert.strictEqual(tokenJson.attributes[0].value, `${project.name}`)


  let attributeCategories = await mainContract.projectAttributeCategories(projectId)
  let categoryIds = attributeCategories.map(ac => ac.toNumber())

  //Validate right number of attributes 
  assert.strictEqual(tokenJson.attributes.length - 1, categoryIds.length) //subtract one for the project name

  //Make sure there's a valid attribute for each category
  for (let categoryId of categoryIds) {

      let categoryName = await wordsContract.word(categoryId)

      let attributeId = await mainContract.tokenAttribute(categoryId, tokenId)
      let attributeName = await wordsContract.word(attributeId)

      //Filter to the right category in the JSON
      let jsonAttribute = tokenJson.attributes.filter(attr => attr.trait_type == categoryName)


      // console.log(`
      //   Category: ${categoryName} (${categoryId.toString()}) / ${jsonAttribute[0].trait_type}
      //   Attribute: ${attributeName} (${attributeId.toString()}) / ${jsonAttribute[0].value}
      // `)


      //Make sure the name is the same as what's in the token json
      assert.strictEqual(jsonAttribute[0].trait_type, categoryName)
      assert.strictEqual(jsonAttribute[0].value, attributeName)


      //Get the valid ones
      let attrProb = await mainContract.getAttributeProbabilities(projectId, categoryId)
      let ids = attrProb.map(ap => ap.attributeId)

      assert.strictEqual(ids.filter(id => id == attributeId).length, 1)

  }


  //Verify ownership
  let balance = await mainContract.balanceOf(operator, tokenId)
  assert.strictEqual(balance.toString(), quantity.toString())





}


async function getJson(tokenId) {

  let uri = await mainContract.uri(tokenId, { from: user0 })

  // 29 = length of "data:application/json;base64,"
  const json = Buffer.from(uri.substring(29), "base64");
  const result = JSON.parse(json.toString());

  return result

}