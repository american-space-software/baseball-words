import { Container } from "inversify";
import { HomeController } from "./controller/home-controller";

import AppComponent from './components/app.f7.html'
import DeployComponent from './components/deploy/index.f7.html'


import { WalletService } from './service/wallet-service';
import { RoutingService } from './service/routing-service';
import { UiService } from './service/ui-service';
import { BaseballsService } from './service/baseballs-service';
import { FindController } from './controller/find-controller';
import { ProjectService } from './service/project-service';

import { providers } from "ethers"
import { ProjectController } from './controller/project-controller';
import { QueueService } from './service/queue_service';
import { TokenService } from './service/token-service';
import { ProfileController } from './controller/profile-controller';
import { PagingService } from './service/paging-service';
import { TokenController } from './controller/token-controller';
import { WordService } from './service/word-service';
import { DeployService } from './service/deploy-service';

let container

function getContainer() {

    if (container) return container 

    container  = new Container()
    
    function framework7() {
        const Framework7 = require('framework7/bundle').default
    
    
        return new Framework7({
            el: '#app', // App root element
            id: 'bwords', // App bundle ID
            name: 'Baseball Words', // App name
            theme: 'auto', // Automatic theme detection
            component: AppComponent
        })
    
    }
    
    
    function contracts() {
        
      const c = require('../contracts.json')

      const overrides = require('../contracts-override.json')


      //Override addresses
      c['MLBC'].address = overrides['MLBC']
      c['Words'].address = overrides['Words']
      c['Baseballs'].address = overrides['Baseballs']
      c['BaseballWords'].address = overrides['BaseballWords']


      return c
    }
    
    function provider() {

        if (typeof window !== "undefined" && window['ethereum']) {
    
            //@ts-ignore
            window.web3Provider = window.ethereum
      
            //@ts-ignore
            return new providers.Web3Provider(web3.currentProvider)  
      
        } else {
            return providers.getDefaultProvider()
        }   
    }
    
    // container.bind('sketch').toConstantValue(sketch())
    container.bind("framework7").toConstantValue(framework7())
    container.bind("contracts").toConstantValue(contracts())
    container.bind("provider").toConstantValue(provider())
    container.bind("name").toConstantValue("Baseball Words")
    
    container.bind(HomeController).toSelf().inSingletonScope()
    container.bind(ProjectController).toSelf().inSingletonScope()
    container.bind(ProfileController).toSelf().inSingletonScope()
    container.bind(TokenController).toSelf().inSingletonScope()

    container.bind(FindController).toSelf().inSingletonScope()
    container.bind(WalletService).toSelf().inSingletonScope()
    container.bind(RoutingService).toSelf().inSingletonScope()
    container.bind(UiService).toSelf().inSingletonScope()
    container.bind(BaseballsService).toSelf().inSingletonScope()
    container.bind(ProjectService).toSelf().inSingletonScope()
    container.bind(QueueService).toSelf().inSingletonScope()
    container.bind(TokenService).toSelf().inSingletonScope()
    container.bind(PagingService).toSelf().inSingletonScope()
    container.bind(WordService).toSelf().inSingletonScope()
    container.bind(DeployService).toSelf().inSingletonScope()

    return container
}



export {
    getContainer, container
}