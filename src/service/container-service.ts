

class ContainerService {

    static getInstance(clazz) {
        
        const getContainer = require("../inversify.config").getContainer

        return getContainer().get(clazz)
    }

    static getDeployInstance(clazz) {

        const getDeployContainer = require("../deploy-inversify.config").getDeployContainer
        return getDeployContainer().get(clazz)
    }

}

export {
    ContainerService
}