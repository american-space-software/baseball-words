import { ethers } from "ethers";
import { injectable } from "inversify";
import { BaseballWordsContract } from "./contracts";
import { WalletService } from "./wallet-service";

@injectable()
class TokenService {

    
    public get baseballWordsContract() : BaseballWordsContract {
        return this.walletService.getContract("BaseballWords")
    }

    constructor(
        private walletService:WalletService
    ) {}

    async getBalance(address) : Promise<number> {
        if (!address) return 0
        return parseInt(await this.baseballWordsContract.balanceOf(address))
    }

    async getProjectId(tokenId) {
        return this.baseballWordsContract.tokenIdToProjectId(tokenId)
    }

    async getMetadata(tokenId) : Promise<any> {

        let uri = await this.getTokenURI(tokenId)
        // 29 = length of "data:application/json;base64,"
        const json = Buffer.from(uri.substring(29), "base64");
        // console.log(json.toString())
        const result = JSON.parse(json.toString());
      

        result.id = tokenId
        // result.animation_url =  result.animation_url.replace("ipfs://", "/ipfs/")

        return result
      
    }

    async getTokenURI(tokenId:string) {
        return this.baseballWordsContract.tokenURI(tokenId)
    }

    async getMetadataByProjectAndIndex(projectId:string, index:number) {
        let tokenId = await this.baseballWordsContract.tokenByProjectAndIndex(projectId, index)

        if (tokenId) {
            return this.getMetadata(tokenId.toString())
        }
                
    }

    async getMetadataByOwnerAndIndex(address:string, index:number) {
        let tokenId = await this.baseballWordsContract.tokenOfOwnerByIndex(address, index)

        if (tokenId) {
            return this.getMetadata(tokenId.toString())
        }
    }

    async getFirstToken(projectId:string) {
        
        try {
            return this.getMetadataByProjectAndIndex(projectId.toString(), 1)
        } catch(ex) {}
    }

    async getProjectTokenList(projectId:string, offset:number=0, limit:number=10)  {
        
        let list = []
        
        for (let i = offset+1 ; i < limit + offset + 1; i++) {
            try {
                let metadata = await this.getMetadataByProjectAndIndex(projectId.toString(), i)
                list.push(metadata)
            } catch(ex) {
                break
            }
            
        }

        return list

    }

    async getProfileTokenList(address:string, offset:number=0, limit:number=10)  {

        let list = []

        for (let i = offset ; i < limit + offset; i++) {

            try {
                let metadata = await this.getMetadataByOwnerAndIndex(address, i)
                list.push(metadata)
            } catch(ex) {
                break
            }
            
        }

        return list

    }


}

export {
    TokenService
}