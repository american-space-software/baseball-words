import { ethers } from "ethers";
import { inject, injectable } from "inversify";
import { PromiseView } from "../util/promise-view";
import { Attribute, BaseballWordsContract, Project, WordsContract } from "./contracts";
import { QueueService } from "./queue_service";
import { TokenService } from "./token-service";
import { WalletService } from "./wallet-service";

@injectable()
class ProjectService {


    public get wordsContract() : WordsContract {
        return this.walletService.getContract("Words")
    }

    public get baseballWordsContract() : BaseballWordsContract {
        return this.walletService.getContract("BaseballWords")
    }

    constructor(
        private walletService:WalletService,
        private queueService:QueueService,
        private tokenService:TokenService,
    ) {}

    
    async getProjects() : Promise<Project[]> {

        let projects:Project[] = []
       
        let projectCount = await this.baseballWordsContract.projectCount()
        
        for (let i=0; i < parseInt(projectCount.toString()); i++) {

            let projectId = await this.baseballWordsContract.projectIds(i)
            projects.push(await this.getProject(projectId))

        }

        return projects
    }

    async getProject(projectId:string) {

        let project:Project = await this.baseballWordsContract.getProject(projectId.toString())
        let first

        if (parseInt(project.minted) > 0) {
            first = await this.tokenService.getFirstToken(projectId)
        }
        
        return {
            id: project.id.toString(),
            name: project.name.toString(),
            description: project.description.toString(),
            maxSupply: project.maxSupply.toString(),
            burnFee: ethers.utils.formatEther(project.burnFee.toString()),
            ipfs: project.ipfs.toString(),
            mintable: project.mintable,
            minted: project.minted,
            available: parseInt(project.minted) < parseInt(project.maxSupply.toString()),
            coverUrl: first ? first.animation_url : undefined 
        }
    }

    async getProjectAttributes(projectId:string) : Promise<Attribute[]> {

        let attributes:Attribute[] = []

        //Get the attribute categories and validate
        let attributeCategories:any = await this.baseballWordsContract.projectAttributeCategories(projectId)

        let attributeCategoryArray = attributeCategories.map(ac => ac.toNumber())

        //Get the attributes and probabilities
        for (let i = 0; i < attributeCategoryArray.length; i++) {

            let categoryId = attributeCategoryArray[i]
            let categoryName = await this.wordsContract.word(categoryId)

            let attributeProbabilities = await this.baseballWordsContract.getAttributeProbabilities(projectId, attributeCategoryArray[i])

            let attribute:Attribute = {
                category: {
                    id: categoryId,
                    name: categoryName
                },
                attributeProbabilities: attributeProbabilities.map(ap => {
                    return {
                        attribute: {
                            id: ap.attributeId,
                        },
                        probability: ap.probability
                    }
                })
            }

            for (let ap of attribute.attributeProbabilities) {
                ap.attribute.name = await this.wordsContract.word(ap.attribute.id)
            }

            attributes.push(attribute)
        }


        attributes.forEach( attribute => {
            attribute.attributeProbabilities.sort( (a, b) => b.probability - a.probability)
        })


        return attributes


    }

    async mint(projectId:string) {
        
        let mintFunction = async () => {
            await this.baseballWordsContract.mint(projectId)
            // this.app.views.main.router.refreshPage()
        }

        let promiseView:PromiseView = {
            title: `Minting Baseball Word. Waiting for transaction to be mined.`,
            promise: mintFunction()
        }

        //Wait for it to be mined
        return this.queueService.queuePromiseView(promiseView)

    }

    async activate(projectId:string) {

        let promiseView:PromiseView = {
            title: `Activating project. Waiting for transaction to be mined.`,
            promise: this.baseballWordsContract.activateProject(projectId)
        }

        //Wait for it to be mined
        return this.queueService.queuePromiseView(promiseView)

    }
    

    async isContractOwner() {
        let owner = ethers.utils.getAddress(await this.baseballWordsContract.owner())

        let address = await this.walletService.getAddress()
        if (!address) return false

        let currentAddress = ethers.utils.getAddress(address)

        
        let isOwner = owner.toString() == currentAddress.toString()

        return isOwner
    }

    // async transferOwnership() {

    //     console.log(this.baseballWordsContract.address)

    //     let promiseView:PromiseView = {
    //         title: `Transferring ownership of projects contract. Waiting for transaction to be mined.`,
    //         promise: this.projectsContract.transferOwnership(this.baseballWordsContract.address)
    //     }

    //     //Wait for it to be mined
    //     return this.queueService.queuePromiseView(promiseView)

    // }

    async createProject(text:string[], // [0] name, [1] description, [2] ipfs
        mintInfo:string[], // [0] maxSupply, [1] burnFee
        attributeCategoryIds:number[],
        attributeProbabilities: number[][]) {

            
            let currentProjects:Project[] = await this.getProjects()
            
            let existing = currentProjects.filter( p => p.name == text[0])
            
            if (existing.length > 0) throw new Error("Project already exists")

            let promiseView:PromiseView = {
                title: `Creating project. Waiting for transaction to be mined.`,
                promise: this.baseballWordsContract.createProject(text, mintInfo, attributeCategoryIds, attributeProbabilities)
            }
    
            //Wait for it to be mined
            return this.queueService.queuePromiseView(promiseView)

    }

}






export {
    ProjectService

}