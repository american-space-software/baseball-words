
interface BaseballWordsContract {
    activateProject(projectId:string)
    mint(projectId:string)
    owner() : string
    tokenURI(tokenId) : string
    tokenByProjectAndIndex(projectId, tokenIndex:number) : string
    tokenOfOwnerByIndex(address:string, index:number) : string
    exists(tokenId) : boolean
    balanceOf(address) : string
    tokenIdToProjectId(tokenId) : string
    address:string
    createProject(text:string[], // [0] name, [1] description, [2] ipfs
        mintInfo:string[], // [0] maxSupply, [1] burnFee
        attributeCategoryIds:number[],
        attributeProbabilities: number[][])

    projectAttributeCategories(projectId) : []
    projectCount() : string
    projectIds(index:number): string
    getProject(projectId:string) : Project
    getAttributeProbabilities(projectId:string, attributeCategoryArray:number[])
}



interface WordsContract{
    word(id:string)
    wordId(word:string)
    createWords(words:string[])
}



interface Project {
    id: string
    name: string
    description: string
    maxSupply: string
    burnFee: string
    ipfs: string
    mintable: string
    minted:string
    attributes?: Attribute[]
    coverUrl:string
}


interface Attribute {
    category: {
        id: string
        name: string
    },
    attributeProbabilities: AttributeProbability[]
}

interface AttributeProbability {
    attribute: {
        id:string
        name:string
    },
    probability: number
}

export {
    WordsContract, BaseballWordsContract, Project, Attribute, AttributeProbability
}