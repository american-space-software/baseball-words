import { ethers } from "ethers";
import { appendFile } from "fs";
import { inject, injectable } from "inversify";
import { PromiseView } from "../util/promise-view";
import { BaseballWordsContract } from "./contracts";
import { QueueService } from "./queue_service";
import { TokenService } from "./token-service";
import { UiService } from "./ui-service";
import { WalletService } from "./wallet-service";

const DEFAULT_BASEBALLS = 10

@injectable()
class BaseballsService {


    public get baseballsContract() : BaseballsContract {
        return this.walletService.getContract("Baseballs")
    }

    public get mlbcContract() : MLBCContract {
        return this.walletService.getContract("MLBC")
    }

    public get baseballWordsContract() : BaseballWordsContract {
        return this.walletService.getContract("BaseballWords")
    }

    constructor(
        private walletService:WalletService,
        private queueService:QueueService,
        private uiService:UiService,
        @inject("framework7") private app
    ) {}

    async getBalance(address:string) {
        if (!address) return '0'
        return ethers.utils.formatEther(await this.baseballsContract.balanceOf(address))
    }

    async getAllowance(address:string) {
        return ethers.utils.formatEther(await this.baseballsContract.allowance(address, this.baseballWordsContract.address))
    }



    async getFindViewModel() {
    
        let findTotal = parseInt(await this.baseballsContract.totalMinted())     
               
        let findMax = 249173
        let findProgress = Math.round( (findTotal * 100) / findMax);

        return {
            findTotal: findTotal,
            findProgress: findProgress,
            findMax: findMax
        }

    }

    async isMinted(tokenId:string) : Promise<boolean> {
        return this.baseballsContract.isMinted(tokenId)
    }

    async mlbcTokenExists(tokenId:string) {
        return this.mlbcContract.exists(tokenId)
    }

    async ownsMlbcToken(tokenId:string) {

        let owner = ethers.utils.getAddress(await this.mlbcContract.ownerOf(tokenId))
        let address = ethers.utils.getAddress(await this.walletService.getAddress())

        if (owner.toString() == address.toString()) {
            return true
        }

        return false

    }

    async mint(tokenIds:any[]) {

        await this.baseballsContract.mint(tokenIds)

        //Mark as minted locally
        for (let tokenId of tokenIds) {
            localStorage.setItem(tokenId, "claimed")
        }

    }

    async mintQueue(tokenIds:number[]) {

        let mintFunction = async (tokenIds) => {
            await this.baseballsContract.mint(tokenIds)
            this.app.views.main.router.refreshPage()
        }

        let promiseView:PromiseView = {
            title: `Minting ${tokenIds.length} BASEBALL tokens`,
            promise: mintFunction(tokenIds)
        }

        //Wait for it to be mined
        return this.queueService.queuePromiseView(promiseView)
    }


    async allow() {

        let allowFunction = async () => {
            let maxApproval = '115792089237316195423570985008687907853269984665640564039457584007913129639935' //(2^256 - 1 )
            await this.baseballsContract.approve(this.baseballWordsContract.address, maxApproval)
            this.app.views.main.router.refreshPage()
        }

        let promiseView:PromiseView = {
            title: `Approving`,
            promise: allowFunction()
        }

        //Wait for it to be mined
        return this.queueService.queuePromiseView(promiseView)
    }


    async autoDetect(amount) : Promise<Set<number>> {

        let walletAddress = await this.walletService.getAddress()
        
        //Get tokens from contract
        this.uiService.showSpinner('Getting list of tokens...')
        let tokens = await this.getOwnersTokens(walletAddress)

        //Turn into an array, parse into integers, and sort
        this.uiService.showSpinner('Finding unclaimed tokens...')
        let tokensArray = Array.from(tokens).map( tokenId => parseInt(tokenId.toString())).sort( (a,b) => a - b )
        
        //Narrow to unclaimed
        let unclaimed = await this.getUnclaimed(tokensArray, amount)

        return unclaimed

    }

    isClaimedLocal(tokenId:string) {
        let value = localStorage.getItem(tokenId.toString())
        if (value == "claimed") return true 
        return false
    }

    async getOwnersTokens(walletAddress:string) : Promise<Set<number>> {
        return this.mlbcContract.tokensOfOwner(walletAddress)
    }

    async getUnclaimed(tokenIds:number[], limit:number) {

        let unclaimed = new Set<number>()

        let i=0
        while (unclaimed.size < limit && i < tokenIds.length) {

            let tokenId = tokenIds[i++]

            //Check if it's already claimed locally
            let isClaimable
            let isClaimed = this.isClaimedLocal(tokenId.toString())

            if (isClaimed) {
                isClaimable = false
            } else {
                isClaimable = await this.isClaimable(tokenId.toString())
            }

            if (isClaimable) {
                console.log(`Adding ${tokenId}`)
                unclaimed.add(tokenId) 
            } else {
                console.log(`${tokenId} is not claimable`)
            } 


        }

        return unclaimed
    }

    async isClaimable(tokenId:string) {
        
        try {
            await this.baseballsContract.validateMintableToken(tokenId.toString())
            return true
        } catch(ex) {
            //mark so we don't try it again
            localStorage.setItem(tokenId.toString(), "claimed")
            console.log(`Marking ${tokenId} claimed`)
        }

        return false
    }

}


interface BaseballsContract {
    totalSupply() : string
    balanceOf(address:string) : string
    totalMinted() : string
    isMinted(tokenId) : boolean
    mint(tokenIds:any[])
    validateMintableToken(tokenId)
    allowance(owner:string, spender:string)
    approve(address:string, maxApproval:string)
}

interface MLBCContract {
    exists(tokenId) : boolean
    ownerOf(tokenId) : string
    tokenOfOwnerByIndex(address:string, index:number) : string
    tokensOfOwner(address:string) : Set<number>
    balanceOf(address:string) : number
}

interface FindViewModel {
    findTotal:number           
    findMax:number
    findProgress:number
}

export {
    BaseballsService,BaseballsContract,FindViewModel
}