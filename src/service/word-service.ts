import { ethers } from "ethers";
import { inject, injectable } from "inversify";
import { PromiseView } from "../util/promise-view";
import { Attribute, BaseballWordsContract, Project, WordsContract } from "./contracts";
import { QueueService } from "./queue_service";
import { TokenService } from "./token-service";
import { WalletService } from "./wallet-service";

@injectable()
class WordService {

    public get wordsContract() : WordsContract {
        return this.walletService.getContract("Words")
    }

    constructor(
        private walletService:WalletService,
        private queueService:QueueService,
    ) {}


    async createWords(words:string[]) {
        console.log("HERERE")
        console.log(this.wordsContract)
        let promiseView:PromiseView = {
            title: `Creating words. Waiting for transaction to be mined.`,
            promise: this.wordsContract.createWords(words)
        }

        //Wait for it to be mined
        return this.queueService.queuePromiseView(promiseView)

    }

    async getWordId(word) {
        let wordId = await this.wordsContract.wordId(word)
        return parseInt(wordId)
    }

}






export {
    WordService

}