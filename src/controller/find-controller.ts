import { injectable } from 'inversify';
import FindIndexComponent from '../components/find/index.f7.html'
import FindCheckComponent from '../components/find/check.f7.html'
import FindMintComponent from '../components/find/mint.f7.html'
import { BaseballsService, FindViewModel } from '../service/baseballs-service';
import { ModelView } from '../util/model-view';
import { routeMap } from '../util/route-map';


@injectable()
class FindController {

    constructor(
        private baseballsService:BaseballsService
    ) {}

    @routeMap("/find")
    async showIndex(): Promise<ModelView> {
        
        return new ModelView(async () => {
            
            let findViewModel:FindViewModel = await this.baseballsService.getFindViewModel()
           
            return {
               findViewModel:findViewModel
            }
  

        }, FindIndexComponent)

    }

    @routeMap("/find/check")
    async showCheck(): Promise<ModelView> {
        
        return new ModelView(async () => {            

        }, FindCheckComponent)

    }

    @routeMap("/find/mint")
    async showClaim(): Promise<ModelView> {
        
        return new ModelView(async () => {            

        }, FindMintComponent)

    }

}

export { FindController }
