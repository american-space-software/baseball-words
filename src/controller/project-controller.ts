import { injectable } from 'inversify';

import ShowProjectComponent from '../components/project/show.f7.html'


import { BaseballsService } from '../service/baseballs-service';
import { Project } from '../service/contracts';
import { PagingService, PagingViewModel } from '../service/paging-service';
import { ProjectService } from '../service/project-service';
import { RouteTo } from '../service/routing-service';
import { TokenService } from '../service/token-service';
import { WalletService } from '../service/wallet-service';
import { ModelView } from '../util/model-view';
import { routeMap } from '../util/route-map';



@injectable()
class ProjectController {

    constructor(
        private projectService:ProjectService,
        private walletService:WalletService,
        private baseballsService:BaseballsService,
        private tokenService:TokenService,
        private pagingService:PagingService
    ) {}

    

    @routeMap("/project/:projectId/:offset")
    async showProject() : Promise<ModelView> {

        return new ModelView(async (routeTo:RouteTo) => {
            
            let project:Project = await this.projectService.getProject(routeTo.params.projectId)

            let address = await this.walletService.getAddress()
            let baseballsBalance = await this.baseballsService.getBalance(address)
            let isContractOwner = await this.projectService.isContractOwner()
            let allowance = await this.baseballsService.getAllowance(address)
            
            let minted = parseInt(project.minted)
            let offset = parseInt(routeTo.params.offset)
        
            let pagingViewModel:PagingViewModel = this.pagingService.buildPagingViewModel(offset, 10, minted)

            let tokens = await this.tokenService.getProjectTokenList(routeTo.params.projectId, pagingViewModel.offset, pagingViewModel.limit)


            return {
                project: project,
                isContractOwner: isContractOwner,
                address: address,
                baseballsBalance: baseballsBalance,
                tokens: tokens,
                allowance: allowance,
                pagingViewModel: pagingViewModel
            }


        }, ShowProjectComponent)
    }

}

export { ProjectController }
