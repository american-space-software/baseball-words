import { injectable } from 'inversify';


import ShowProfileComponent from '../components/profile/show.f7.html'


import { BaseballsService } from '../service/baseballs-service';
import { PagingService, PagingViewModel } from '../service/paging-service';
import { RouteTo } from '../service/routing-service';
import { TokenService } from '../service/token-service';
import { ModelView } from '../util/model-view';
import { routeMap } from '../util/route-map';



@injectable()
class ProfileController {

    constructor(
        private baseballsService:BaseballsService,
        private tokenService:TokenService,
        private pagingService:PagingService
    ) {}

    

    @routeMap("/profile/:address/:offset")
    async showProfile() : Promise<ModelView> {

        return new ModelView(async (routeTo:RouteTo) => {
            
            let address = routeTo.params.address
            
            let baseballsBalance = await this.baseballsService.getBalance(routeTo.params.address)

            let balance = await this.tokenService.getBalance(address)
            let offset = parseInt(routeTo.params.offset)
        
            let pagingViewModel:PagingViewModel = this.pagingService.buildPagingViewModel(offset, 10, balance)

            let tokens = await this.tokenService.getProfileTokenList(routeTo.params.address, pagingViewModel.offset, pagingViewModel.limit)

            return {
                address: address,
                baseballsBalance: baseballsBalance,
                tokens: tokens,
                pagingViewModel: pagingViewModel
            }


        }, ShowProfileComponent)
    }

}

export { ProfileController }
