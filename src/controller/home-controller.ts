import { injectable } from 'inversify';

import HomeComponent from '../components/home/home.f7.html'

import { BaseballsService, FindViewModel } from '../service/baseballs-service';
import { Project } from '../service/contracts';
import { ProjectService } from '../service/project-service';
import { RouteTo } from '../service/routing-service';
import { WalletService } from '../service/wallet-service';
import { ModelView } from '../util/model-view';
import { routeMap } from '../util/route-map';

require("../html/images/roadmap.png")


@injectable()
class HomeController {

    constructor(
        private baseballsService:BaseballsService,
        private projectService:ProjectService,
        private walletService:WalletService
    ) {}

    @routeMap("/")
    async showIndex(): Promise<ModelView> {
        
        return new ModelView(async () => {
            
            let projects:Project[] = await this.projectService.getProjects()
            
            let findViewModel:FindViewModel = await this.baseballsService.getFindViewModel()
            
    
            let isContractOwner = await this.projectService.isContractOwner()
            let address = await this.walletService.getAddress()
            
            
            let baseballsBalance = '0'
            let allowance = '0'

            if (address) {
                baseballsBalance = await this.baseballsService.getBalance(address)
                allowance = await this.baseballsService.getAllowance(address)
            }

            let coverUrl

            if (projects?.length > 0) {
                coverUrl = projects[2].coverUrl
                coverUrl =  coverUrl?.replace("ipfs://", "/ipfs/")
            }

            

            return {
                findViewModel: findViewModel,
                projects: projects,
                isContractOwner: isContractOwner,
                baseballsBalance: baseballsBalance,
                address: address,
                allowance: allowance,
                coverUrl: coverUrl
            }
  

        }, HomeComponent)

    }


}

export { HomeController }
