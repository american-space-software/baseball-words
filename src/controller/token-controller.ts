import { injectable } from 'inversify';


import ShowTokenComponent from '../components/token/show.f7.html'
import { ProjectService } from '../service/project-service';

import { RouteTo } from '../service/routing-service';
import { TokenService } from '../service/token-service';
import { ModelView } from '../util/model-view';
import { routeMap } from '../util/route-map';



@injectable()
class TokenController {

    constructor(
        private tokenService:TokenService,
        private projectService:ProjectService
    ) {}

    

    @routeMap("/token/:tokenId")
    async showProfile() : Promise<ModelView> {

        return new ModelView(async (routeTo:RouteTo) => {
            
            let tokenId = routeTo.params.tokenId
            
            let token = await this.tokenService.getMetadata(tokenId)
            let projectId = await this.tokenService.getProjectId(tokenId)

            let project = await this.projectService.getProject(projectId)

            
            token.animation_url =  token.animation_url?.replace("ipfs://", "/ipfs/")


            return {
                token: token,
                project: project
            }


        }, ShowTokenComponent)
    }

}

export { TokenController }
