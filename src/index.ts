import "core-js/stable"
import "regenerator-runtime/runtime"
import "reflect-metadata"


import { getContainer } from "./inversify.config"


//Import CSS
import './html/css/framework7-bundle.min.css'
import './html/css/framework7-icons.css'
import './html/css/app.css'
import './html/ipfs/QmPXMXk3tMwzEAnt16dtzv8pbYWjgULX1kcucVRy84F3ba'
import './html/ipfs/QmV5HxsFCutsfJta6bKmzBQyhpmvnB5GYztRX3Gs9RobdQ'
import './html/ipfs/QmPdEmBPta68Q8zGc95RmtGDUdrU4ETK78rMoC1ZvFmWYV'

import './html/images/bbwords.png'
import './html/images/preview.png'
import './html/images/profile.png'
import './html/images/logo.jpg'


import { RoutingService } from "./service/routing-service"

export default async () => {

    let container = getContainer()
  
    let app = container.get("framework7")
    let routingService:RoutingService = container.get(RoutingService)

    //Initialize routing
    app.routes = routingService.buildRoutesForContainer(container)

}
