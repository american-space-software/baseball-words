import { Container } from "inversify";

import DeployAppComponent from './components/deploy.f7.html'
import DeployComponent from './components/deploy/index.f7.html'


import { WalletService } from './service/wallet-service';
import { RoutingService } from './service/routing-service';
import { UiService } from './service/ui-service';
import { BaseballsService } from './service/baseballs-service';
import { ProjectService } from './service/project-service';

import { providers } from "ethers"
import { QueueService } from './service/queue_service';
import { TokenService } from './service/token-service';
import { PagingService } from './service/paging-service';
import { WordService } from './service/word-service';
import { DeployService } from './service/deploy-service';

let deployContainer


function getDeployContainer() {

    if (deployContainer) return deployContainer 

    deployContainer  = new Container()

    
    function deployFramework7() {
    
        const Framework7 = require('framework7/bundle').default
    
    
        return new Framework7({
            el: '#app', // App root element
            id: 'bwords', // App bundle ID
            name: 'Deploy', // App name
            theme: 'auto', // Automatic theme detection
            routes: [{
                path: "/deploy",
                component: DeployComponent
            }],
            component: DeployAppComponent
        })
    
    }
    
    function deployContracts() {
      const c = require('../contracts.json')
      const overrides = require('../contracts-override.json')

        //Override addresses
        c['MLBC'].address = overrides['MLBC']
        c['Words'].address = overrides['Words']
        c['Baseballs'].address = overrides['Baseballs']
        c['BaseballWords'].address = overrides['BaseballWords']

      return c
    }
    
    function deployProvider() {

        if (typeof window !== "undefined" && window['ethereum']) {
    
            //@ts-ignore
            window.web3Provider = window.ethereum
      
            //@ts-ignore
            return new providers.Web3Provider(web3.currentProvider)  
      
        }    
    }
    
    deployContainer.bind("framework7").toConstantValue(deployFramework7())
    deployContainer.bind("contracts").toConstantValue(deployContracts())
    deployContainer.bind("provider").toConstantValue(deployProvider())
    deployContainer.bind("name").toConstantValue("Deploy")
 

    deployContainer.bind(WalletService).toSelf().inSingletonScope()
    deployContainer.bind(RoutingService).toSelf().inSingletonScope()
    deployContainer.bind(UiService).toSelf().inSingletonScope()
    deployContainer.bind(BaseballsService).toSelf().inSingletonScope()
    deployContainer.bind(ProjectService).toSelf().inSingletonScope()
    deployContainer.bind(QueueService).toSelf().inSingletonScope()
    deployContainer.bind(TokenService).toSelf().inSingletonScope()
    deployContainer.bind(PagingService).toSelf().inSingletonScope()
    deployContainer.bind(WordService).toSelf().inSingletonScope()
    deployContainer.bind(DeployService).toSelf().inSingletonScope()

    return deployContainer
}


export {
    getDeployContainer
}